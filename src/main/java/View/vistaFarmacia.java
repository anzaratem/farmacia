/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package View;

import Controller.Producto;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author familia zarate
 */
public class vistaFarmacia extends javax.swing.JFrame {

    /**
     * Creates new form vistaFarmacia
     */
    public vistaFarmacia() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jRadioButton1 = new javax.swing.JRadioButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jLabelFarmacia = new javax.swing.JPanel();
        jLabelNombreFarm = new javax.swing.JLabel();
        jLabelInicio = new javax.swing.JLabel();
        jLabelFarmacias = new javax.swing.JLabel();
        jLabelSeleccion = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jSeparator1 = new javax.swing.JSeparator();
        jLabelHospital = new javax.swing.JLabel();
        jLabelLogo = new javax.swing.JLabel();
        jPanelProductos = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableProductos = new javax.swing.JTable();
        jLabelNombre = new javax.swing.JLabel();
        jLabelId = new javax.swing.JLabel();
        jLabelTemperatura = new javax.swing.JLabel();
        jLabelValorBase = new javax.swing.JLabel();
        jTextFieldNombre = new javax.swing.JTextField();
        jTextFieldId = new javax.swing.JTextField();
        jTextFieldTemperatura = new javax.swing.JTextField();
        jTextFieldValorBase = new javax.swing.JTextField();
        jButtonGuardar = new javax.swing.JButton();
        jButtonActualizar = new javax.swing.JButton();
        jButtonEliminar = new javax.swing.JButton();
        jButtonConsultar = new javax.swing.JButton();
        jButtonLimpiar = new javax.swing.JButton();
        jButtonSalir = new javax.swing.JButton();

        jRadioButton1.setText("jRadioButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setMinimumSize(new java.awt.Dimension(640, 340));

        jLabelFarmacia.setBackground(new java.awt.Color(255, 255, 255));
        jLabelFarmacia.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelNombreFarm.setFont(new java.awt.Font("Roboto Medium", 0, 18)); // NOI18N
        jLabelNombreFarm.setText("FARMACIA MISION TIC");
        jLabelFarmacia.add(jLabelNombreFarm, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 10, 200, 30));

        jLabelInicio.setFont(new java.awt.Font("Roboto Black", 1, 24)); // NOI18N
        jLabelInicio.setText("INICIAR SESION");
        jLabelFarmacia.add(jLabelInicio, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, -1, -1));

        jLabelFarmacias.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        jLabelFarmacias.setText("FARMACIA");
        jLabelFarmacia.add(jLabelFarmacias, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, -1, -1));

        jLabelSeleccion.setForeground(new java.awt.Color(204, 204, 204));
        jLabelSeleccion.setText("Seleccione la farmacia deseada: ");
        jLabelFarmacia.add(jLabelSeleccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, 320, -1));

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Sede Norte", "Sede Sur", "Sede Oriente", "Sede Occidente" }));
        jLabelFarmacia.add(jComboBox1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 210, 320, -1));
        jLabelFarmacia.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 193, 350, 10));

        jLabelHospital.setIcon(new javax.swing.ImageIcon("C:\\Users\\familia zarate\\Desktop\\Reto5\\reto5_2\\Hospital.png")); // NOI18N
        jLabelHospital.setText("jLabel2");
        jLabelFarmacia.add(jLabelHospital, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 40, 250, 270));

        jLabelLogo.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        jLabelLogo.setIcon(new javax.swing.ImageIcon("C:\\Users\\familia zarate\\Desktop\\Reto5\\reto5_2\\favicon.png")); // NOI18N
        jLabelLogo.setText("FARMACIA");
        jLabelFarmacia.add(jLabelLogo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 160, 30));

        jTabbedPane1.addTab("Farmacias", jLabelFarmacia);

        jPanelProductos.setBackground(new java.awt.Color(0, 153, 204));

        jTableProductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Nombre", "Id", "Temperatura", "Valor Base", "Costo"
            }
        ));
        jTableProductos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableProductosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableProductos);

        jLabelNombre.setFont(new java.awt.Font("Roboto Black", 0, 12)); // NOI18N
        jLabelNombre.setText("Nombre");

        jLabelId.setFont(new java.awt.Font("Roboto Black", 0, 12)); // NOI18N
        jLabelId.setText("Id");

        jLabelTemperatura.setFont(new java.awt.Font("Roboto Black", 0, 12)); // NOI18N
        jLabelTemperatura.setText("Temperatura");

        jLabelValorBase.setFont(new java.awt.Font("Roboto Black", 1, 12)); // NOI18N
        jLabelValorBase.setText("Valor Base");

        jTextFieldNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldNombreActionPerformed(evt);
            }
        });

        jButtonGuardar.setFont(new java.awt.Font("Roboto Black", 0, 12)); // NOI18N
        jButtonGuardar.setText("Guardar");
        jButtonGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonGuardarActionPerformed(evt);
            }
        });

        jButtonActualizar.setFont(new java.awt.Font("Roboto Black", 0, 12)); // NOI18N
        jButtonActualizar.setText("Actualizar");
        jButtonActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonActualizarActionPerformed(evt);
            }
        });

        jButtonEliminar.setFont(new java.awt.Font("Roboto Black", 0, 12)); // NOI18N
        jButtonEliminar.setText("Eliminar");
        jButtonEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEliminarActionPerformed(evt);
            }
        });

        jButtonConsultar.setFont(new java.awt.Font("Roboto Black", 0, 12)); // NOI18N
        jButtonConsultar.setText("Consultar");
        jButtonConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonConsultarActionPerformed(evt);
            }
        });

        jButtonLimpiar.setFont(new java.awt.Font("Roboto Black", 0, 12)); // NOI18N
        jButtonLimpiar.setText("Limpiar");
        jButtonLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonLimpiarActionPerformed(evt);
            }
        });

        jButtonSalir.setFont(new java.awt.Font("Roboto Black", 0, 12)); // NOI18N
        jButtonSalir.setText("Salir");
        jButtonSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelProductosLayout = new javax.swing.GroupLayout(jPanelProductos);
        jPanelProductos.setLayout(jPanelProductosLayout);
        jPanelProductosLayout.setHorizontalGroup(
            jPanelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelProductosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelProductosLayout.createSequentialGroup()
                        .addGroup(jPanelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelTemperatura)
                            .addComponent(jLabelId, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelNombre, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelValorBase, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(35, 35, 35)
                        .addGroup(jPanelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextFieldNombre, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
                            .addComponent(jTextFieldId)
                            .addComponent(jTextFieldTemperatura)
                            .addComponent(jTextFieldValorBase)))
                    .addGroup(jPanelProductosLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(jPanelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonSalir)
                            .addComponent(jButtonLimpiar)))
                    .addGroup(jPanelProductosLayout.createSequentialGroup()
                        .addGap(0, 170, Short.MAX_VALUE)
                        .addGroup(jPanelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jButtonActualizar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonEliminar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonConsultar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonGuardar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17))
        );
        jPanelProductosLayout.setVerticalGroup(
            jPanelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelProductosLayout.createSequentialGroup()
                .addGroup(jPanelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelProductosLayout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addGroup(jPanelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelNombre)
                            .addComponent(jTextFieldNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelId)
                            .addComponent(jTextFieldId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelTemperatura)
                            .addComponent(jTextFieldTemperatura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelValorBase)
                            .addComponent(jTextFieldValorBase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonGuardar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonActualizar)
                            .addComponent(jButtonLimpiar))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonEliminar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonConsultar)
                            .addComponent(jButtonSalir)))
                    .addGroup(jPanelProductosLayout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(37, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Productos", jPanelProductos);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGuardarActionPerformed
        // TODO add your handling code here:
        String nombre = jTextFieldNombre.getText();
        double temperatura = Double.parseDouble(jTextFieldTemperatura.getText());
        double valorBase = Double.parseDouble(jTextFieldValorBase.getText());
        Producto p = new Producto();
        //p.setId(id);
        p.setNombre(nombre);
        p.setTemperatura(temperatura);
        p.setValorBase(valorBase); 
        if (p.guardarProducto()) {
            JOptionPane.showMessageDialog(this, "Producto guardado con éxito\n" + p.toString());
        } else {
            JOptionPane.showMessageDialog(this, "Error al guardar");
        }
        limpiarFormularioProductos();
        
    }//GEN-LAST:event_jButtonGuardarActionPerformed

    private void jButtonActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonActualizarActionPerformed
        // TODO add your handling code here:
        String nombre = jTextFieldNombre.getText();
        int id = Integer.parseInt(jTextFieldId.getText());
        double temperatura = Double.parseDouble(jTextFieldTemperatura.getText());
        double valorBase = Double.parseDouble(jTextFieldValorBase.getText());
        Producto p = new Producto();
        p.setNombre(nombre);
        p.setId(id);
        p.setTemperatura(temperatura);
        p.setValorBase(valorBase);
        if (p.actualizarProducto()) {
            JOptionPane.showMessageDialog(this, "Operacion realizada con éxito");
        } else {
            JOptionPane.showMessageDialog(this, "Operacion fallida");
        } 
    }//GEN-LAST:event_jButtonActualizarActionPerformed

    private void jButtonEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEliminarActionPerformed
        // TODO add your handling code here:
        int id = Integer.parseInt(jTextFieldId.getText());
        Producto p = new Producto();
        p.setId(id);
        if (p.eliminarProducto()) {
            JOptionPane.showMessageDialog(this, "Operacion realizada con éxito");
        } else {
            JOptionPane.showMessageDialog(this, "Operacion fallida");
        }
    }//GEN-LAST:event_jButtonEliminarActionPerformed

    private void jButtonConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonConsultarActionPerformed
        // TODO add your handling code here:
        Object[][] rowData = new Object[1][5];
        Object[] columnsNames = {"Nombre", "Id", "Temperatura", "ValorBase", "Costo"};
        DefaultTableModel model = new DefaultTableModel();
        model.setColumnIdentifiers(columnsNames);
        jTableProductos.setModel(model);
        Producto p = new Producto();
        List<Producto> lista = p.listarProductos();
        for (Producto pro : lista) {
            rowData[0][0] = pro.getNombre();
            rowData[0][1] = pro.getId();
            rowData[0][2] = pro.getTemperatura();
            rowData[0][3] = pro.getValorBase();
            rowData[0][4] = pro.calcularCosto();
            model.addRow(rowData[0]);
        }
        limpiarFormularioProductos();
        
    }//GEN-LAST:event_jButtonConsultarActionPerformed

    private void limpiarFormularioProductos(){
        jTextFieldId.setText("");
        jTextFieldNombre.setText("");
        jTextFieldTemperatura.setText("");
        jTextFieldValorBase.setText("");
    }
    
    private void jTableProductosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableProductosMouseClicked
        // TODO add your handling code here:
        
        int index = jTableProductos.getSelectedRow();
               jTextFieldNombre.setText(jTableProductos.getModel().getValueAt(index, 0).toString());
               jTextFieldId.setText(jTableProductos.getModel().getValueAt(index, 1).toString());
               jTextFieldTemperatura.setText(jTableProductos.getModel().getValueAt(index, 2).toString());
               jTextFieldValorBase.setText(jTableProductos.getModel().getValueAt(index, 3).toString());

        /*       
        String nombre = jTextFieldNombre.getText();
        int id = Integer.parseInt(jTextFieldId.getText());
        double temperatura = Double.parseDouble(jTextFieldTemperatura.getText());
        double valorBase = Double.parseDouble(jTextFieldValorBase.getText());*/
    }//GEN-LAST:event_jTableProductosMouseClicked

    private void jButtonLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonLimpiarActionPerformed
        // TODO add your handling code here:
        jTextFieldNombre.setText(null);
        jTextFieldId.setText(null);
        jTextFieldTemperatura.setText(null);
        jTextFieldValorBase.setText(null);

    }//GEN-LAST:event_jButtonLimpiarActionPerformed

    private void jButtonSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalirActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_jButtonSalirActionPerformed

    private void jTextFieldNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldNombreActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(vistaFarmacia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(vistaFarmacia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(vistaFarmacia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(vistaFarmacia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new vistaFarmacia().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonActualizar;
    private javax.swing.JButton jButtonConsultar;
    private javax.swing.JButton jButtonEliminar;
    private javax.swing.JButton jButtonGuardar;
    private javax.swing.JButton jButtonLimpiar;
    private javax.swing.JButton jButtonSalir;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JPanel jLabelFarmacia;
    private javax.swing.JLabel jLabelFarmacias;
    private javax.swing.JLabel jLabelHospital;
    private javax.swing.JLabel jLabelId;
    private javax.swing.JLabel jLabelInicio;
    private javax.swing.JLabel jLabelLogo;
    private javax.swing.JLabel jLabelNombre;
    private javax.swing.JLabel jLabelNombreFarm;
    private javax.swing.JLabel jLabelSeleccion;
    private javax.swing.JLabel jLabelTemperatura;
    private javax.swing.JLabel jLabelValorBase;
    private javax.swing.JPanel jPanelProductos;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTableProductos;
    private javax.swing.JTextField jTextFieldId;
    private javax.swing.JTextField jTextFieldNombre;
    private javax.swing.JTextField jTextFieldTemperatura;
    private javax.swing.JTextField jTextFieldValorBase;
    // End of variables declaration//GEN-END:variables
}
